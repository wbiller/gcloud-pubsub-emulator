FROM google/cloud-sdk:alpine

RUN apk --update add openjdk8-jre
RUN gcloud components install --quiet beta pubsub-emulator
RUN mkdir -p /var/pubsub

VOLUME /var/pubsub

ENV PORT ${EMULATOR_PORT:-8085}

EXPOSE $PORT

CMD gcloud beta emulators pubsub start --data-dir=/var/pubsub --host-port=0.0.0.0:$PORT --log-http --verbosity=debug --user-output-enabled
