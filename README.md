# Google Cloud PubSub Emulator

An emulator for Google Cloud PubSub.

# How to to use this image

## Start the emulator

```bash
docker run --name pubsub-emulator -e EMULATOR_PORT=8085 -p8085:8085 -d wbiller/gcloud-pubsub-emulator
```

# Enviroment variables

## `EMULATOR_PORT`

With this variable you can set the port to use.
